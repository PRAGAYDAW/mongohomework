﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace OFMMongo.Controllers
{
    public class AbstractController : Controller
    {
        // GET: Abstract
        public virtual  string ConnectionString
        {
            get { return WebConfigurationManager.ConnectionStrings["Mongo"].ConnectionString; }
        }
    }
}