﻿using MongoDB.Bson;
using MongoDB.Driver;
using OFMMongo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using OFMMongo.Models.Repository;
using System.Net;

namespace OFMMongo.Controllers
{
    public class HomeController : AbstractController
    {
        public ActionResult Index()
        {
            JsonData data = new JsonData();
            data.listjson = new ContactRepository().GetContact();
            return View("Index", data);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Name,Major,Subject")] json data)
        {
            if (ModelState.IsValid)
            {
                new ContactRepository().InsertContact(data);
                return RedirectToAction("Index");
            }
            return View(data);
        }


        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            json data = new ContactRepository().GetContact().Where(a => a.id == id).FirstOrDefault();
            if (data == null)
            {
                return HttpNotFound();
            }
            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "id,Name,Major,Subject")] json data)
        {
            JsonData contactdata = new JsonData();

            if (ModelState.IsValid)
            {
                new ContactRepository().UpdateContact(data);
                contactdata.listjson = new ContactRepository().GetContact();
                return RedirectToAction("Index", contactdata.listjson);
            }
            return View(contactdata.json);
        }

        public ActionResult Delete(string id)
        {
            json data = new json();
            data = new ContactRepository().GetContact().Where(a => a.id == id).FirstOrDefault();
            if (data == null)
            {
                return HttpNotFound();
            }
            return View("Delete", data);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            new ContactRepository().DelectContact(id);
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}