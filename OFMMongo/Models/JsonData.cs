﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OFMMongo.Models
{
    public class JsonData
    {
        public IEnumerable<json> listjson { get; set; }
        public json json { get; set; }
    }

    public class json
    {
        public string id { get; set; }
        public string Name { get; set; }
        public string Major { get; set; }
        public string Subject { get; set; }

    }
}
