﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OFMMongo.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OFMMongo.Models.Repository
{
    public class ContactRepository : AbstractController
    {
        public IEnumerable<json> GetContact()
        {
            List<json> datalist = new List<json>();
            var client = new MongoClient(ConnectionString);
            var db = client.GetDatabase("test");
            var server = client.GetServer();

            var collection = db.GetCollection<BsonDocument>("test");
            var doc = collection.Find(a => true).ToList();


            foreach (var item in doc.Select(a => a.Elements))
            {
                json data = new json();
                data.id = item.FirstOrDefault().Value.AsBsonValue.ToString();
                data.Name = item.FirstOrDefault(a => a.Name == "name").Value.AsBsonValue.ToString();
                data.Major = item.FirstOrDefault(a => a.Name == "major").Value.AsBsonValue.ToString();
                data.Subject = item.FirstOrDefault(a => a.Name == "subject").Value.AsBsonValue.ToString();

                datalist.Add(data);
            }

            return datalist;
        }

        public void InsertContact(json data)
        {
            var client = new MongoClient(ConnectionString);
            var db = client.GetDatabase("test");
            var server = client.GetServer();
            var collection = db.GetCollection<BsonDocument>("test");
            var document = new BsonDocument { { "name", data.Name }, { "major", data.Major }, { "subject", data.Subject } };
            collection.InsertOneAsync(document);

        }

        public json UpdateContact(json data)
        {
            var client = new MongoClient(ConnectionString);
            var db = client.GetDatabase("test");
            var server = client.GetServer();
            var collection = db.GetCollection<BsonDocument>("test");


            var builder = Builders<BsonDocument>.Filter;
            //var filter = builder.Eq("_id", new ObjectId(data.id));
            var update = Builders<BsonDocument>.Update
                        .Set("name", data.Name)
                        .Set("major", data.Major)
                        .Set("subject", data.Subject);
            collection.FindOneAndUpdateAsync(Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(data.id)), update);

            return data;
        }


        public void DelectContact(string id)
        {
            var client = new MongoClient(ConnectionString);
            var db = client.GetDatabase("test");
            var server = client.GetServer();
            var collection = db.GetCollection<BsonDocument>("test");

            collection.FindOneAndDeleteAsync(Builders<BsonDocument>.Filter.Eq("_id",new ObjectId(id)));
        }
    }
}