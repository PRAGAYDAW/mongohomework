﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OFMMongo.Startup))]
namespace OFMMongo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
